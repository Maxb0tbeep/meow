# Meow, a better AUR helper

![meow](https://gitlab.com/Maxb0tbeep/meow/-/raw/main/meow.png?ref_type=heads)

AUR package coming when it's ready™

## Prerequisites 

You need nim installed to compile this. If you don't have it:

`# pacman -S nim`

It's also recommended to use a font in your terminal that supports Nerd Icons and Ligatures. [Here's why.](font.md) 

A good one is `FiraCode Nerd`

`# pacman -S ttf-firacode-nerd`

## Building Manually

`$ mkdir build`

`$ nimble build meow`

The compiled binary will be at build/meow. You can test it by running `$ ./build/meow help`

## Using Meow

You can get started with Meow by running the following command:

`$ meow help`
