# Package

version       = "1.0.0"
author        = "spacelucy"
description   = "A better AUR helper"
license       = "GPL-3.0-or-later"
srcDir        = "src"
binDir        = "build"
bin           = @["meow"]


# Dependencies

requires "nim >= 2.0.2"
requires "termstyle >= 0.1.0"
