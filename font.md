# Font
Why you should use a font with Nerd Icons and Ligatures with Meow:

Screenshot with correct font:

![correct](https://i.imgur.com/4bijsJh.png)

Screenshot with incorrect font:

![incorrect](https://i.imgur.com/7Nygv7x.png)

As you can see, the arrows that look like -> turn into → (along with other symbols)

The arrow pointing down and to the right also looks weird with some fonts.

It's not a huge difference, and I definitely recommend it, but hey, it's your computer
