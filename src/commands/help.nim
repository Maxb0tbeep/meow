import std/[parsecfg, streams]
import termstyle

proc Help*(): void =
  const version = staticRead("../../meow.nimble").newStringStream.loadConfig.getSectionValue("", "version") # https://stackoverflow.com/a/77463816

  echo "\n", # this is kinda weird lol
    " /\\____/\\   ___   ___  _   __   _ \n".bold.white, 
    " | ".bold.white,"O  O".bold.cyan," |  |___  |   |  \\  /\\  /\n".bold.white,
    " >  v   <  |___  |___|   \\/  \\/ \n".bold.white,
    "__________________________________\n".bold.magenta,
    "                           v".green, version.green, "\n"

  echo "$".yellow," meow...\n\n",
    "-> ".bold.cyan,"install"," / ".bold.red,"-S","                  |".bold.magenta," install pacman/AUR package(s)\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," install <package>\n".cyan,
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," -S <package>\n\n".cyan,
    "-> ".bold.cyan,"(package name)","                |".bold.magenta," search and install a package. similar to other AUR helpers (yay, paru)\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," <package>\n\n".cyan,
    "-> ".bold.cyan,"remove"," / ".bold.red,"-R","                   |".bold.magenta," remove pacman/AUR package(s)\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," remove <package>\n".cyan,
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," -R <package>\n\n".cyan,
    "-> ".bold.cyan,"up"," / ".bold.red,"update"," / ".bold.red,"(no args)", "       |".bold.magenta," update system repositories and upgrade packages\n",
    " ↳ ".bold.green,"$".bold.yellow," meow\n".blue,
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," update\n\n".cyan,
    "-> ".bold.cyan,"clean"," / ".bold.red,"-Sc", "                   |".bold.magenta," clear pacman and AUR package cache\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," clean\n\n".cyan,
    "-> ".bold.cyan,"refresh"," / ".bold.red,"-Sy", "                 |".bold.magenta," synchronize pacman repositories\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," refresh\n\n".cyan,
    "-> ".bold.cyan,"help", "                          |".bold.magenta," print this command\n",
    " ↳ ".bold.green,"$".bold.yellow," meow".blue," help".cyan,
    "\n\nFlags:\n".yellow.bold,
    " -> ".bold.green, "-v", " / ".red.bold, "--verbose", "               |".bold.magenta, " output additional command information. will spam your terminal."

