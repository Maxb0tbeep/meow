import std/[json, strformat, os, unicode, strutils, times, sequtils]
import termstyle
import ../utils/[pkg, query, common]

proc Install*(): void =
  var
    packageCount:int
    packageIndex:int
    packages: seq[string] 
    packageRepo: seq[string]
    pkgResults: seq[JsonNode] = newSeq[JsonNode]()
    validPkg:bool = true
    plural:string
    outOfDate:string
    repoColor = white
    error:string
    verbose:bool = false
    flagCount: int
    flags: seq[string]

  # add packages from params to a sequence and count too
  for i in countup(2,paramCount()): # start on 2 because "install" is also a param
    let charSeq = toSeq(paramStr(i).items)

    if charSeq[0] != '-':
      inc packageCount
      add(packages, paramStr(i))
    else:
      inc flagCount
      add(flags, paramStr(i))

  # check how many packages to install and verify that theres >0 too
  if packageCount == 0:
    echo " No package specified".red
    return
  elif packageCount > 1:
    plural = "s"
  else:
    plural = ""

  echo "Fetching package information...".cyan # make an animated loading icon thing for stuff like this

  # check if packages are valid and break if they are not
  
  for i in countup(0, len(flags)-1):
    if flags[i] == "-v" or flags[i] == "--verbose":
      verbose = true
    else:
      echo fmt"Unknown flag: {flags[i]}".yellow

  for i in countup(0,len(packages)-1):
    try:
      for pkg in packages:
        var pkgDuplicates:int

        if(packages[i] ==  pkg):
          inc(pkgDuplicates)
        if(pkgDuplicates > 1):
          Error("Duplicate packages to install. Make sure each package is only listed once")
          return

      let queryResult = Query(packages[i])

      add(packageRepo, queryResult["Repo"].getStr)
      add(pkgResults, queryResult)
    except:
      Error(fmt"Package ""{packages[i]}"" is not a valid package.")
      validPkg = false
      break

  if validPkg:
    echo "Installing the following ".yellow, packageCount.green.bold, fmt" package{plural}:".yellow

    for pkg in packages:
      let
        pkgResults:JsonNode = pkgResults[packageIndex] # get the results again
        version:string = pkgResults["Version"].getStr 
        maintainer:string = pkgResults["Maintainer"].getStr
        outOfDate_epoch:int = pkgResults["OutOfDate"].getInt
        installed:string = pkgResults["Installed"].getStr

      if outOfDate_epoch > 0:
        let time = fromUnix(outOfDate_epoch).format("yyyy-MM-dd")
        outOfDate = fmt"(Outdated: {time})"

        if maintainer == "":
          outOfDate = fmt"(Orphaned: {time})"

      case packageRepo[packageIndex]
        of "core", "core-testing":
          repoColor = yellow
        of "extra", "extra-testing":
          repoColor = red
        of "multilib", "multilib-testing":
          repoColor = magenta
        else:
          repoColor = cyan # AUR and custom repos


      echo "-> ".bold.green, packageRepo[packageIndex].repoColor, "/".white, pkg.bold.blue, fmt" ({version})".cyan, fmt"{installed}".green, fmt" {outOfDate}".red # echo each package to be installed
      inc(packageIndex) 

    if Prompt("Confirm installation", yellow):
      for i in countup(0,len(packages)-1):
        if(packageRepo[i] == "aur"):
          error = InstallAUR(packages[i], verbose)
        else:
          error = InstallPacman(packages[i], verbose)

        if error != "0":
          echo error.red

          if not verbose:
            echo fmt"run the previous command again with".magenta," -v".cyan," or ".magenta,"--verbose".cyan," for more information.".magenta
          break
      if error == "0":
        Success(fmt"{packageCount} Package{plural} installed successfully")
    else:
      Success("Package installation canceled")
