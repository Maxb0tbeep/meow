import std/[os, osproc, strformat, math]
import ../utils/[pkg, common]
import termstyle

proc GetDirSize(path:string): float =
  return round(toFloat(getFileSize(path)) / 1024 / 1024, 2)

proc Clean*(): void =
  SanityCheck()

  const
    pacmanPath:string = "/var/cache/pacman/pkg"
    reposPath:string = "/var/lib/pacman/sync"
  let 
    user:string = getEnv("USER")
    meowPath:string = fmt"/home/{user}/.cache/meow"
    meowSize:float = GetDirSize(meowPath)
    pacmanSize:float = GetDirSize(pacmanPath)
    reposSize:float = GetDirSize(reposPath)
  var
    meow:bool = false
    pacman:bool = false
    repos:bool = false
    err:int
 
  if Prompt("Delete meow package cache? "&fmt"({meowSize} MB) ".blue.bold&fmt"({meowPath})".cyan.bold, red): meow = true
  if Prompt("Delete unused packages from pacman cache? "&fmt"({pacmanSize} MB) ".blue.bold&fmt"({pacmanPath})".cyan.bold, red): pacman = true
  if Prompt("Delete unused pacman repositories? "&fmt"({reposSize} MB) ".blue.bold&fmt"({reposPath})".cyan.bold, red): repos = true
  
  if meow or pacman or repos:
    echo "\nWill NOT be removed:\n".green, "-> ".blue, "Currently installed packages"

    echo "\nWill be removed:".red
    if meow: echo "-> ".blue, "Meow AUR package cache (where AUR packages are built/compiled)"
    if pacman: echo "-> ".blue, "Unused packages from cache (old versions or uninstalled)"
    if repos: echo "-> ".blue, "Unused repositories from cache\n"
  
    if Prompt("Is this okay?", yellow):
      echo "Cleaning...".cyan.bold
      if meow: err = execCmd(fmt"rm -rf {meowPath}/*")
      if pacman: err = execCmd(fmt"sudo rm -rf {pacmanPath}/*")
      if repos:
        err = execCmd(fmt"sudo rm -rf {reposPath}/*")
        echo "Your system will not be able to install packages until repos are synced again.".red
        echo fmt"run".yellow, fmt" ""meow repos""".blue.bold " or".yellow, fmt" ""pacman -Sy""".blue.bold, " to sync".yellow
  else:
    echo "Nothing to do.".yellow
