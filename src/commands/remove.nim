import std/[strformat, os, json, sequtils]
import ../utils/[query, common]
import termstyle

proc Remove*(): void =
  try:
    let 
      queryResult = Query(paramStr(2))
      pkgName:string = queryResult["Name"].getStr
      pkgVersion:string = queryResult["Version"].getStr
      pkgRepo:string = queryResult["Repo"].getStr

    if Prompt(fmt"Confirm removal of {pkgRepo}/{pkgName}-{pkgVersion}", yellow):
      echo "remove"
    else: 
      echo "don't"
  except:
    Error(fmt"Could not find package: {paramStr(2)}")
