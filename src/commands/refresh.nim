import std/[osproc, strformat]
import termstyle
import ../utils/common

proc Refresh*(): void =
  echo "->".blue," Updating Repositories...".cyan
  let err = execCmdEx("sudo pacman -Sy")
  if (err.exitCode == 0):
    Success("Up to date!")
  else:
    Error(fmt"Error code {err.exitCode}")
