import std/[os, strformat]
import commands/[help, install, clean, refresh, remove]
import termstyle

when isMainModule:
  let user:string = getEnv("USER")

  if user == "root":
    echo "Do not run Meow as root! Exiting.".red.bold
    quit()

  case paramStr(1):
    of "help":
      Help()
    of "install", "-S":
      Install()
    of "clean", "-Sc":
      Clean()
    of "refresh", "-Sy":
      Refresh()
    of "remove", "-R":
      Remove()
    else:
      if paramCount() == 0:
        echo "TODO: update system"
      else:
        echo fmt"Invalid operation. Run ".red, "meow help".yellow.bold " for information on how to use meow".red
