import std/[json, osproc, httpclient, strformat, strutils]

var
  repo:string
  name:string
  version:string
  maintainer:string
  outOfDate_epoch:int
  installed:string

const localPkgDir:string = fmt"/var/lib/pacman/local"

proc Query*(package: string): JsonNode =
  let pacmanRepoPkgs = execCmdEx(fmt"pacman -Sl | grep "" {package} """)

  if pacmanRepoPkgs.exitCode == 0: # check if package exists on pacman repos
    let pkg = pacmanRepoPkgs.output.split()
    repo = pkg[0]
    name = pkg[1]
    version = pkg[2]
    maintainer = "N/A"
    outOfDate_epoch = 0

    installed = fmt" {pkg[3]}"
  else: # try AUR otherwise
    var client = newHttpClient()
    try:
      let 
        response:JsonNode = parseJson(client.getContent(fmt"https://aur.archlinux.org/rpc/v5/info/{package}"))["results"]
        pacmanInstalled = execCmdEx(fmt"pacman -Q | grep ""{package}""").exitCode

      repo = "aur"
      name = response[0]["Name"].getStr
      version = response[0]["Version"].getStr
      maintainer = response[0]["Maintainer"].getStr
      outOfDate_epoch = response[0]["OutOfDate"].getInt

      if pacmanInstalled == 0:
        installed = " [installed]"
    finally:
      client.close()

  let packageData:JsonNode = %* [
    {
      "Repo": repo,
      "Name": name,
      "Version":version,
      "Maintainer":maintainer,
      "OutOfDate":outOfDate_epoch,
      "Installed": installed
    }
  ]

  return(packageData[0])
