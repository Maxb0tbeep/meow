import std/[strutils, strformat]
import termstyle

proc Prompt*(question:string, arrowColor:proc): bool =
  stdout.write "-> ".bold.arrowColor, question, " [".white, "Y".green.bold, "/".white, "n".red.bold, "] ".white
  let confirmation = toLower(readLine(stdin))

  if confirmation == "y" or confirmation == "":
    return true
  else:
    return false

proc Success*(msg:string): void =
    echo fmt" {msg}".green

proc Warn*(msg:string): void =
    echo fmt" {msg}".yellow

proc Error*(msg:string): void =
    echo fmt" {msg}".red
