import std/[os, osproc, strformat, strutils]

import termstyle

const
  user:string = getEnv("USER")
  cacheDir:string = fmt"/home/{user}/.cache/meow"

proc PkgHeader(package:string): void = # just looks nice. adds the "-> [pkg] "
  stdout.write "-> ".blue, "[".white, package.cyan.bold, "] ".white

proc SanityCheck*(): void =
  if not dirExists(cacheDir):
    echo "->".green.bold, fmt" The directory ""{cacheDir}"" does not exist! Creating...".cyan
    createDir(cacheDir)

proc InstallAUR* (package: string, verbose:bool): string =
  SanityCheck()

  PkgHeader(package)
  if dirExists(fmt"{cacheDir}/{package}"): # check if need to clone or just pull
    echo "Updating PKGBUILD from AUR...".green
    let clone = execCmdEx(fmt"cd {cacheDir}/{package} && git pull")
    if verbose: echo clone.output
  else:
    echo "Cloning PKGBUILD from AUR...".green
    let clone = execCmdEx(fmt"cd {cacheDir} && git clone https://aur.archlinux.org/{package}.git && cd {package}")
    if verbose: echo clone.output

  PkgHeader(package)
  echo "Building with makepkg".blue
  
  let
    makepkg = execCmdEx(fmt"cd {cacheDir}/{package} && makepkg -si --noprogressbar --noconfirm")
    errorCode = makepkg.exitCode

  if verbose: echo makepkg.output

  var error:string

  if errorCode != 0:
    case errorCode: # error codes from https://man.archlinux.org/man/makepkg.
      of 1:
        error = "Unknown cause of failure."
      of 2:
        error = "Error in configuration file."
      of 3:
        error = "User specified an invalid option."
      of 4:
        error = "Error in user-supplied function in PKGBUILD."
      of 5:
        error = "Failed to create a viable package."
      of 6:
        error = "A source or auxiliary file specified in the PKGBUILD is missing."
      of 7:
        error = "The PKGDIR is missing."
      of 8:
        error = "Failed to install dependencies."
      of 9:
        error = "Failed to remove dependencies."
      of 10:
        error = "User attempted to run makepkg as root."
      of 11:
        error = "User lacks permissions to build or install to a given location."
      of 12:
        error = "Error parsing PKGBUILD."
      of 13:
        error = "A package has already been built."
      of 14:
        error = "The package failed to install."
      of 15:
        error = "Programs necessary to run makepkg are missing."
      of 16:
        error = "Specified GPG key does not exist or failed to sign package."
      else:
        error = "Unknown error meaning."

    return fmt" Error {errorCode}: {error}"
  else:
    return "0"

proc InstallPacman* (package:string, verbose:bool): string =
  PkgHeader(package)
  echo "Installing with pacman".blue
  let pacman = execCmdEx(fmt"sudo pacman -S {package} --noconfirm --noprogressbar")
  if verbose: echo pacman.output

  if pacman.exitCode == 0:
    return "0"
  else:
    return fmt" Error {intToStr(pacman.exitCode)}"
